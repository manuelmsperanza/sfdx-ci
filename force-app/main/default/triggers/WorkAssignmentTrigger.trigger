trigger WorkAssignmentTrigger on WorkAssignment__c (after insert, after update, after delete) {

    for(WorkAssignment__c curWorkAssignment : Trigger.new) {
        if(Trigger.isDelete){
            curWorkAssignment.Engagement__c = 0;
        }
        WorkAssignmentController.checkNewAssignment(curWorkAssignment);
    }    
}