trigger CapacityPlanListenerTrigger on CapacityPlanEvent__e (after insert) {

    insert new CapacityPlanTriggerRun__c(Name='CapacityPlanListenerTrigger', BatchRowCount__c=Trigger.new.size());

    List<Id> listCase = new List<Id>();
    for (CapacityPlanEvent__e event : Trigger.new) {
        listCase.add(event.Case_Id__c);
    }

    Map<Id, Case> mapCase = new Map<Id, Case>([SELECT Id, UpdateCounter__C FROM Case WHERE Id in :listCase]);

    for (CapacityPlanEvent__e event : Trigger.new) {
        if(mapCase.get(event.Case_Id__c).UpdateCounter__C == null){
            mapCase.get(event.Case_Id__c).UpdateCounter__C = 1;
        } else {
            mapCase.get(event.Case_Id__c).UpdateCounter__C = mapCase.get(event.Case_Id__c).UpdateCounter__C + 1;
        }
    }

    update mapCase.values();

}