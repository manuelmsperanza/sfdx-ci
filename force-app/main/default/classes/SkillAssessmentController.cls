public with sharing class SkillAssessmentController {

    @AuraEnabled(cacheable=true)
    public static List<WorkerSkill__c> getRelatedSkills(Id workerId) {
        List<WorkerSkill__c> listUserSkill = [SELECT Id, Name, Skill__c FROM WorkerSkill__c 
            WHERE Worker__r.Id = :workerId];
        Map<String, WorkerSkill__c> mapUserSkills = new Map<String, WorkerSkill__c>();
        for(WorkerSkill__c curEmployeeSkill : listUserSkill) {
            mapUserSkills.put(curEmployeeSkill.Name, curEmployeeSkill);
        }
        List<WorkerSkill__c> returnListUserSkill = new List<WorkerSkill__c>();
        for(Skill__c curSkill : [SELECT Id, Name, Description__c FROM Skill__c]) {
            WorkerSkill__c employeeSkill = null;
            if((employeeSkill = mapUserSkills.get(curSkill.Name)) == null) {
                employeeSkill = new WorkerSkill__c(Name=curSkill.Name, Skill__c=curSkill.Id, Worker__c=workerId);
                
            }
            returnListUserSkill.add(employeeSkill);
        }

        return returnListUserSkill;
    }

}