@isTest
private class WorkAssignmentControllerTest {
    
    @TestSetup
    static void makeData(){

        List<Worker__c> workForce = new List<Worker__c>();

        Worker__c buHead1 = new Worker__c(Name='BUHead1');
        workForce.add(buHead1);

        Worker__c pm1 = new Worker__c(Name='PM1');
        workForce.add(pm1);

        Worker__c teamLead1 = new Worker__c(Name='TeamLead1');
        workForce.add(teamLead1);

        Worker__c analyst1 = new Worker__c(Name='Analyst1');
        workForce.add(analyst1);

        Worker__c analyst2 = new Worker__c(Name='Analyst2');
        workForce.add(analyst2);

        Worker__c analyst3 = new Worker__c(Name='Analyst3');
        workForce.add(analyst3);

        Worker__c analyst4 = new Worker__c(Name='Analyst4');
        workForce.add(analyst4);

        Worker__c teamLead2 = new Worker__c(Name='TeamLead2');
        workForce.add(teamLead2);

        Worker__c consultant1 = new Worker__c(Name='Consultant1');
        workForce.add(consultant1);

        Worker__c consultant2 = new Worker__c(Name='Consultant2');
        workForce.add(consultant2);

        Worker__c consultant3 = new Worker__c(Name='Consultant3');
        workForce.add(consultant3);

        Worker__c consultant4 = new Worker__c(Name='Consultant4');
        workForce.add(consultant4);

        Worker__c pm2 = new Worker__c(Name='PM2');
        workForce.add(pm2);

        Worker__c teamLead3 = new Worker__c(Name='TeamLead3');
        workForce.add(teamLead3);

        Worker__c developer1 = new Worker__c(Name='Developer1');
        workForce.add(developer1);

        Worker__c developer2 = new Worker__c(Name='Developer2');
        workForce.add(developer2);

        Worker__c developer3 = new Worker__c(Name='Developer3');
        workForce.add(developer3);

        Worker__c teamLead4 = new Worker__c(Name='TeamLead4');
        workForce.add(teamLead4);

        Worker__c tester1 = new Worker__c(Name='Tester1');
        workForce.add(tester1);

        Worker__c tester2 = new Worker__c(Name='Tester2');
        workForce.add(tester2);

        Worker__c tester3 = new Worker__c(Name='Tester3');
        workForce.add(tester3);

        Worker__c tester4 = new Worker__c(Name='Tester4');
        workForce.add(tester4);

        Worker__c pm3 = new Worker__c(Name='PM3');
        workForce.add(pm3);

        Worker__c teamLead5 = new Worker__c(Name='TeamLead5');
        workForce.add(teamLead5);

        Worker__c operation1 = new Worker__c(Name='Operation1');
        workForce.add(operation1);

        Worker__c operation2 = new Worker__c(Name='Operation2');
        workForce.add(operation2);

        Worker__c operation3 = new Worker__c(Name='Operation3');
        workForce.add(operation3);

        insert workForce;

        Account account = new Account(Name='Account');
        insert account;

        List<Project__c> projects = new List<Project__c>();
        
        Date firstOfFeb = Date.newInstance(Date.today().year(), 2, 1);
        Date endNextYear = Date.newInstance(Date.today().year()+1, 12, 31);
        
        Project__c project1 = createProject(account, 'Project 1', firstOfFeb, endNextYear, 'Medium');
        projects.add(project1);
        
        Date hNewYear = Date.newInstance(Date.today().year(), 1, 1);
        Project__c project2 = createProject(account, 'Project 2', hNewYear, hNewYear.addMonths(6).addDays(-1), 'Medium');
        projects.add(project2);

        Project__c project3 = createProject(account, 'Project 3', hNewYear, hNewYear.addMonths(13).addDays(-1), 'Medium');
        projects.add(project3);

        Project__c project4 = createProject(account, 'Project 4', null, null, 'Medium');
        projects.add(project4);

        Date aprilFool = Date.newInstance(Date.today().year(), 4, 1);
        Project__c project5 = createProject(account, 'Project 5', aprilFool, aprilFool.addMonths(11), 'Medium');
        projects.add(project5);

        Date firstOfMay = Date.newInstance(Date.today().year(), 5, 1);
        Project__c project6 = createProject(account, 'Project 6', firstOfMay, firstOfMay.addMonths(11), 'Medium');
        projects.add(project6);

        Project__c project7 = createProject(account, 'Project 7', firstOfMay, null, 'Medium');
        projects.add(project7);

        insert projects;

        List<WorkPackage__c> workPackages = new List<WorkPackage__c>();
        
        WorkPackage__c wp11 = createWorkPackage(project1, 'Work Package 1.1', firstOfFeb, firstOfFeb.addMonths(3).addDays(-1), 'Analisys');
        workPackages.add(wp11);

        WorkPackage__c wp12 = createWorkPackage(project1, 'Work Package 1.2', firstOfFeb, firstOfFeb.addMonths(5).addDays(-1), 'Consulting');
        workPackages.add(wp12);

        WorkPackage__c wp13 = createWorkPackage(project1, 'Work Package 1.3', aprilFool, aprilFool.addMonths(2).addDays(-1), 'Development');
        workPackages.add(wp13);
        
        Date firstOfJun = Date.newInstance(Date.today().year(), 6, 1);
        WorkPackage__c wp14 = createWorkPackage(project1, 'Work Package 1.4', firstOfJun, firstOfJun.addMonths(4).addDays(-1), 'Testing');
        workPackages.add(wp14);

        Date firstOfOct = Date.newInstance(Date.today().year(), 10, 1);
        WorkPackage__c wp15 = createWorkPackage(project1, 'Work Package 1.5', firstOfOct, null, 'Support');
        workPackages.add(wp15);

        WorkPackage__c wp21 = createWorkPackage(project2, 'Work Package 2.1', hNewYear, null, 'Support');
        workPackages.add(wp21);

        WorkPackage__c wp22 = createWorkPackage(project2, 'Work Package 2.2', firstOfJun, firstOfJun.addMonths(1).addDays(-1), 'AM');
        workPackages.add(wp22);

        WorkPackage__c wp31 = createWorkPackage(project3, 'Work Package 3.1', hNewYear, hNewYear.addMonths(4).addDays(-1), 'Consulting');
        workPackages.add(wp31);

        Date firstOfMar = Date.newInstance(Date.today().year(), 3, 1);
        WorkPackage__c wp32 = createWorkPackage(project3, 'Work Package 3.2', firstOfMar, firstOfMar.addMonths(3).addDays(-1), 'Analisys');
        workPackages.add(wp32);

        WorkPackage__c wp33 = createWorkPackage(project3, 'Work Package 3.3', firstOfMay, firstOfMay.addMonths(5).addDays(-1), 'Development');
        workPackages.add(wp33);
        
        Date firstOfJul = Date.newInstance(Date.today().year(), 7, 1);
        WorkPackage__c wp34 = createWorkPackage(project3, 'Work Package 3.4', firstOfJul, firstOfJul.addMonths(5).addDays(-1), 'Testing');
        workPackages.add(wp34);

        Date firstOfNov = Date.newInstance(Date.today().year(), 11, 1);
        WorkPackage__c wp35 = createWorkPackage(project3, 'Work Package 3.5', firstOfNov, firstOfNov.addMonths(5).addDays(-1), 'AM');
        workPackages.add(wp35);

        Date hNewNextYear = Date.newInstance(Date.today().year()+1, 1, 1);
        WorkPackage__c wp36 = createWorkPackage(project3, 'Work Package 3.6', hNewNextYear, null, 'Support');
        workPackages.add(wp36);

        WorkPackage__c wp41 = createWorkPackage(project4, 'Work Package 4.1', aprilFool, null, 'Analisys');
        workPackages.add(wp41);

        WorkPackage__c wp51 = createWorkPackage(project5, 'Work Package 5.1', aprilFool, null, 'Analisys');
        workPackages.add(wp51);

        insert workPackages;

        List<WorkActivity__c> workActivities = new List<WorkActivity__c>();

        WorkActivity__c wa111 = createWorkActivity(wp11, 'Work Activity 1.1.1', firstOfFeb, firstOfNov.addMonths(1).addDays(-1));
        workActivities.add(wa111);

        Date midFeb = Date.newInstance(Date.today().year(), 2, 15);
        WorkActivity__c wa112 = createWorkActivity(wp11, 'Work Activity 1.1.2', midFeb, midFeb.addMonths(1).addDays(-1));
        workActivities.add(wa112);

        WorkActivity__c wa113 = createWorkActivity(wp11, 'Work Activity 1.1.3', firstOfMar, firstOfMar.addMonths(1).addDays(-1));
        workActivities.add(wa113);

        WorkActivity__c wa121 = createWorkActivity(wp12, 'Work Activity 1.2.1', firstOfFeb, firstOfFeb.addMonths(5).addDays(-1));
        workActivities.add(wa121);

        WorkActivity__c wa131 = createWorkActivity(wp13, 'Work Activity 1.3.1', aprilFool, aprilFool.addMonths(2).addDays(-1));
        workActivities.add(wa131);

        WorkActivity__c wa141 = createWorkActivity(wp14, 'Work Activity 1.4.1', firstOfJun, firstOfJun.addMonths(4).addDays(-1));
        workActivities.add(wa141);

        WorkActivity__c wa151 = createWorkActivity(wp15, 'Work Activity 1.5.1', firstOfOct, null);
        workActivities.add(wa151);

        WorkActivity__c wa211 = createWorkActivity(wp21, 'Work Activity 2.1.1', hNewYear, null);
        workActivities.add(wa211);

        WorkActivity__c wa221 = createWorkActivity(wp22, 'Work Activity 2.2.1', firstOfJun, firstOfJun.addMonths(1).addDays(-1));
        workActivities.add(wa221);

        WorkActivity__c wa311 = createWorkActivity(wp31, 'Work Activity 3.1.1', hNewYear, firstOfJun.addMonths(4).addDays(-1));
        workActivities.add(wa311);

        WorkActivity__c wa321 = createWorkActivity(wp32, 'Work Activity 3.2.1', firstOfMar, firstOfMar.addMonths(3).addDays(-1));
        workActivities.add(wa321);

        WorkActivity__c wa331 = createWorkActivity(wp33, 'Work Activity 3.3.1', firstOfMay, firstOfMay.addMonths(5).addDays(-1));
        workActivities.add(wa331);

        WorkActivity__c wa341 = createWorkActivity(wp34, 'Work Activity 3.4.1', firstOfJul, firstOfJul.addMonths(5).addDays(-1));
        workActivities.add(wa341);

        WorkActivity__c wa351 = createWorkActivity(wp35, 'Work Activity 3.5.1', firstOfNov, firstOfNov.addMonths(5).addDays(-1));
        workActivities.add(wa351);

        WorkActivity__c wa361 = createWorkActivity(wp36, 'Work Activity 3.6.1', hNewNextYear, null);
        workActivities.add(wa361);

        WorkActivity__c wa411 = createWorkActivity(wp41, 'Work Activity 4.1.1', aprilFool, aprilFool.addMonths(1).addDays(-1));
        workActivities.add(wa411);

        insert workActivities;

        List<WorkAssignment__c> workAssignments = new List<WorkAssignment__c>();

        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa111.Id, Worker__c=analyst1.Id, Engagement__c=100, StartDate__c=firstOfMar,EndDate__c=null, Approval_Status__c='Approved'));
        Date thisStartOfWeek = Date.today().toStartOfWeek();
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa111.Id, Worker__c=analyst1.Id, Engagement__c=100, StartDate__c=thisStartOfWeek, EndDate__c=thisStartOfWeek.addDays(6)));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa112.Id, Worker__c=analyst2.Id, Engagement__c=50, StartDate__c=midFeb, EndDate__c=midFeb.addMonths(1).addDays(-1), Approval_Status__c='Approved'));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa112.Id, Worker__c=analyst1.Id, Engagement__c=50, StartDate__c=midFeb, EndDate__c=midFeb.addMonths(1).addDays(-1), Approval_Status__c='Approved'));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa113.Id, Worker__c=analyst2.Id, Engagement__c=50, StartDate__c=firstOfMar, EndDate__c=midFeb.addMonths(1).addDays(-1), Approval_Status__c='Approved'));
        Date midMar = Date.newInstance(Date.today().year(), 3, 15);
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa113.Id, Worker__c=analyst2.Id, Engagement__c=100, StartDate__c=midMar, EndDate__c=firstOfMar.addMonths(1).addDays(-1), Approval_Status__c='Approved'));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa113.Id, Worker__c=analyst3.Id, Engagement__c=100, StartDate__c=midMar, EndDate__c=firstOfMar.addMonths(1).addDays(-1), Approval_Status__c='Approved'));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa121.Id, Worker__c=consultant1.Id, Engagement__c=100, StartDate__c=firstOfFeb, EndDate__c=firstOfFeb.addMonths(5).addDays(-1), Approval_Status__c='Approved'));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa121.Id, Worker__c=consultant1.Id, Engagement__c=100, StartDate__c=thisStartOfWeek, EndDate__c=thisStartOfWeek.addDays(6)));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa121.Id, Worker__c=consultant2.Id, Engagement__c=100, StartDate__c=firstOfFeb, EndDate__c=firstOfFeb.addMonths(5).addDays(-1), Approval_Status__c='Approved'));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa121.Id, Worker__c=consultant3.Id, Engagement__c=100, StartDate__c=firstOfFeb, EndDate__c=firstOfFeb.addMonths(3).addDays(-1), Approval_Status__c='Approved'));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa121.Id, Worker__c=consultant3.Id, Engagement__c=50, StartDate__c=firstOfMay, EndDate__c=firstOfMay.addMonths(2).addDays(-1), Approval_Status__c='Approved'));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa121.Id, Worker__c=consultant4.Id, Engagement__c=50, StartDate__c=firstOfMay, EndDate__c=firstOfMay.addMonths(2).addDays(-1), Approval_Status__c='Approved'));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa131.Id, Worker__c=developer1.Id, Engagement__c=100, StartDate__c=aprilFool, EndDate__c=aprilFool.addMonths(2).addDays(-1), Approval_Status__c='Approved'));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa131.Id, Worker__c=developer2.Id, Engagement__c=70, StartDate__c=aprilFool, EndDate__c=aprilFool.addMonths(2).addDays(-1), Approval_Status__c='Approved'));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa131.Id, Worker__c=developer3.Id, Engagement__c=30, StartDate__c=aprilFool, EndDate__c=aprilFool.addMonths(2).addDays(-1), Approval_Status__c='Approved'));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa141.Id, Worker__c=tester1.Id, Engagement__c=100, StartDate__c=firstOfJun, EndDate__c=firstOfJun.addMonths(4).addDays(-1), Approval_Status__c='Approved'));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa141.Id, Worker__c=tester2.Id, Engagement__c=100, StartDate__c=firstOfJun, EndDate__c=firstOfJun.addMonths(4).addDays(-1), Approval_Status__c='Approved'));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa141.Id, Worker__c=tester3.Id, Engagement__c=100, StartDate__c=firstOfJun, EndDate__c=firstOfJun.addMonths(4).addDays(-1), Approval_Status__c='Approved'));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa141.Id, Worker__c=tester4.Id, Engagement__c=100, StartDate__c=firstOfJun, EndDate__c=firstOfJun.addMonths(4).addDays(-1), Approval_Status__c='Approved'));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa151.Id, Worker__c=operation1.Id, Engagement__c=50, StartDate__c=firstOfOct, EndDate__c=null, Approval_Status__c='Approved'));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa151.Id, Worker__c=operation2.Id, Engagement__c=50, StartDate__c=firstOfOct, EndDate__c=null, Approval_Status__c='Approved'));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa211.Id, Worker__c=operation1.Id, Engagement__c=100, StartDate__c=hNewYear, EndDate__c=hNewYear.addMonths(9).addDays(-1), Approval_Status__c='Approved'));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa211.Id, Worker__c=operation1.Id, Engagement__c=50, StartDate__c=firstOfOct, EndDate__c=null, Approval_Status__c='Approved'));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa211.Id, Worker__c=operation3.Id, Engagement__c=100, StartDate__c=firstOfOct, EndDate__c=null, Approval_Status__c='Approved'));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa221.Id, Worker__c=operation2.Id, Engagement__c=100, StartDate__c=firstOfJun,EndDate__c=firstOfJun.addMonths(1).addDays(-1), Approval_Status__c='Approved'));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa221.Id, Worker__c=analyst1.Id, Engagement__c=100, StartDate__c=midMar, EndDate__c=firstOfMar.addMonths(1).addDays(-1)));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa311.Id, Worker__c=consultant1.Id, Engagement__c=100, StartDate__c=hNewYear, EndDate__c=hNewYear.addMonths(4).addDays(-1), Approval_Status__c='Approved'));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa321.Id, Worker__c=analyst1.Id, Engagement__c=100, StartDate__c=firstOfMar, EndDate__c=firstOfMar.addMonths(3).addDays(-1), Approval_Status__c='Approved'));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa331.Id, Worker__c=developer1.Id, Engagement__c=100, StartDate__c=firstOfMay, EndDate__c=firstOfMay.addMonths(5).addDays(-1), Approval_Status__c='Approved'));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa341.Id, Worker__c=tester1.Id, Engagement__c=100, StartDate__c=firstOfJul, EndDate__c=firstOfJul.addMonths(5).addDays(-1)));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa351.Id, Worker__c=operation1.Id, Engagement__c=100, StartDate__c=firstOfNov, EndDate__c=firstOfNov.addMonths(5).addDays(-1)));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa361.Id, Worker__c=operation1.Id, Engagement__c=100, StartDate__c=hNewNextYear, EndDate__c=null));

        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa411.Id, Worker__c=analyst1.Id, Engagement__c=100, StartDate__c=aprilFool, EndDate__c=aprilFool.addMonths(1).addDays(-1), Approval_Status__c='Approved'));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa411.Id, Worker__c=analyst2.Id, Engagement__c=50, StartDate__c=aprilFool, EndDate__c=aprilFool.addMonths(1).addDays(-1), Approval_Status__c='Approved'));
        workAssignments.add(new WorkAssignment__c(WorkActivity__c=wa411.Id, Worker__c=analyst3.Id, Engagement__c=100, StartDate__c=aprilFool, EndDate__c=aprilFool.addMonths(1).addDays(-1), Approval_Status__c='Approved'));

        insert workAssignments;

    }

    private static Project__c createProject(Account account, String name, Date startDate, Date endDate, String priority){

        Date actualStartDate = null;
        Date actualEndDate = null;
        String status = 'New';

        if(startDate != null && Date.today().daysBetween(startDate) <= 0) {
            actualStartDate = startDate;
            status = 'In Progress';
        }
        if(endDate != null && Date.today().daysBetween(endDate) <= 0){
            actualEndDate = endDate;
            status = 'Closed';
        }

        Project__c project = new Project__c(
            Account__c = account.Id,
            Name=name,
            PlannedStartDate__c=startDate,
            PlannedEndDate__c=endDate,
            ActualStartDate__c=actualStartDate,
            ActualEndDate__c= actualEndDate,
            Priority__c='Medium',
            Status__c=status);
        
        return project;

    }

    private static WorkPackage__c createWorkPackage(Project__c project, String name, Date startDate, Date endDate, String type){
        Date actualStartDate = null;
        Date actualEndDate = null;
        String status = 'New';

        if(startDate != null && Date.today().daysBetween(startDate) <= 0) {
            actualStartDate = startDate;
            status = 'In Progress';
        }
        if(endDate != null && Date.today().daysBetween(endDate) <= 0){
            actualEndDate = endDate;
            status = 'Closed';
        }

        WorkPackage__c workPackage = new WorkPackage__c(
            Project__c = project.Id,
            Name=name,
            PlannedStartDate__c=startDate,
            PlannedEndDate__c=endDate,
            ActualStartDate__c=actualStartDate,
            ActualEndDate__c= actualEndDate,
            Type__c=type,
            Status__c=status);
        
        return workPackage;
    }

    private static WorkActivity__c createWorkActivity(WorkPackage__c workpackage, String name, Date startDate, Date endDate){

        Date actualStartDate = null;
        Date actualEndDate = null;
        String status = 'New';

        if(startDate != null && Date.today().daysBetween(startDate) <= 0) {
            actualStartDate = startDate;
            status = 'In Progress';
        }
        if(endDate != null && Date.today().daysBetween(endDate) <= 0){
            actualEndDate = endDate;
            status = 'Closed';
        }

        WorkActivity__c workActivity = new WorkActivity__c(
            Project__c = workpackage.Project__c,
            WorkPackage__c = workpackage.Id,
            Name=name,
            PlannedStartDate__c=startDate,
            PlannedEndDate__c=endDate,
            ActualStartDate__c=actualStartDate,
            ActualEndDate__c= actualEndDate,
            Status__c=status);
        
        return workActivity;

    }

    @isTest private static void testGetUsersAssigments(){
        Date startDate = Date.newInstance(Date.today().year(), 2, 1).toStartOfWeek();
        Date endDate = startDate.addMonths(7).toStartOfWeek().addDays(-1);

        String columns = '[{"label":"Worker","fieldName":"Worker"}';

        for(Date curDate = Date.newInstance(Date.today().year(), 2, 1).toStartOfWeek(); curDate.daysBetween(endDate) >= 0; curDate = curDate.addDays(7)) {
            String month = curDate.month().format();
            String day = curDate.day().format();
            String formattedDate = curDate.year() + '-' + month.leftPad(2,'0') + '-' + day.leftPad(2,'0');
            columns = columns + ',{"label":"' + formattedDate + '","fieldName":"' + formattedDate + '"}';
        }

        columns = columns + ']';

        Test.startTest();
        WorkAssignmentController.getUsersAssigments(startDate, endDate, 'week', columns);
        Test.stopTest();

    }

    @isTest private static void testDeleteWorkAssignments(){
        
        List<WorkAssignment__c> workAssignments = [SELECT Id FROM WorkAssignment__c WHERE Worker__r.Name='Analyst1'];

        Test.startTest();
        delete workAssignments;
        Test.stopTest();
    }

    @isTest private static void testGetUsersAssigmentsDetail(){
        Date startDate = Date.newInstance(Date.today().year(), 2, 1).toStartOfWeek();
        Date endDate = startDate.addMonths(7).toStartOfWeek().addDays(-1);
        Test.startTest();
        List<WorkAssignment__c> workAssignments = WorkAssignmentController.getUserAssigmentsDetail(startDate, endDate, '["Analyst1"]');
        Test.stopTest();
    }

    @isTest private static void testGetUsersAssigmentsDetailNoEndDate(){
        Date startDate = Date.newInstance(Date.today().year(), 2, 1).toStartOfWeek();
        Date endDate = null;
        Test.startTest();
        List<WorkAssignment__c> workAssignments = WorkAssignmentController.getUserAssigmentsDetail(startDate, endDate, '["Analyst1"]');
        Test.stopTest();
    }

    @isTest private static void testGetProjects(){
         
        Test.startTest();
        List<Project__c> projects = WorkAssignmentController.getProjects();
        Test.stopTest();

        System.assertEquals(7, projects.size());

    }

    @isTest private static void testGetWorkPackages(){
        
        Project__c project = [SELECT Id FROM Project__c WHERE Name = 'Project 1'];

        Test.startTest();
        List<WorkPackage__c> workPackages = WorkAssignmentController.getWorkPackages(project.Id);
        Test.stopTest();

        System.assertEquals(5, workPackages.size());

    }

    @isTest private static void testGetWorkActivities(){
        
        WorkPackage__c workPackage = [SELECT Id FROM WorkPackage__c WHERE Name = 'Work Package 1.1'];

        Test.startTest();
        List<WorkActivity__c> workActivities = WorkAssignmentController.getWorkActivities(workPackage.Id);
        Test.stopTest();

        System.assertEquals(3, workActivities.size());

    }


}