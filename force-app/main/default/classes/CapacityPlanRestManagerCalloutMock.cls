@isTest
global class CapacityPlanRestManagerCalloutMock implements HttpCalloutMock {
    
    private final String baseUrl = 'https://org.my.salesforce.com/services/apexrest/CapacityPlan';

    global HTTPResponse respond(HTTPRequest req) {
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setStatusCode(200);

        RestRequest restReq = new RestRequest(); 
        restReq.requestURI = req.getEndpoint();
        restReq.httpMethod = req.getMethod();
        if(req.getBody() != null){
            restReq.requestBody = Blob.valueOf(req.getBody());
        }
        RestContext.request = restReq;
        RestContext.response = new RestResponse();

        switch on req.getMethod() {
            when 'GET' {
                System.assert(req.getEndpoint().startsWith(this.baseUrl), 'Endpoint must start with ' + this.baseUrl);
                Case returnCase = CapacityPlanRestManager.getCaseById();
                res.setBody(JSON.serialize(returnCase));
            }
            when 'POST' {
                System.assertEquals(this.baseUrl, req.getEndpoint());
                Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.getBody());
                ID caseId = CapacityPlanRestManager.createCase(params.get('subject').toString(), params.get('status').toString(), params.get('origin').toString(), params.get('priority').toString());
                res.setBody(caseId);        
            }
            when 'PUT' {
                System.assertEquals(this.baseUrl, req.getEndpoint());
                Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.getBody());
                String idParam = null;
                if(params.containsKey('id')){
                    idParam = params.get('id').toString();
                }
                ID caseId = CapacityPlanRestManager.upsertCase(params.get('subject').toString(), params.get('status').toString(), params.get('origin').toString(), params.get('priority').toString(), idParam);
                res.setBody(caseId);
            }
            when 'DELETE' {
                System.assert(req.getEndpoint().startsWith(this.baseUrl), 'Endpoint must start with ' + this.baseUrl);
                CapacityPlanRestManager.deleteCase();
            }
            when 'PATCH' {
                System.assert(req.getEndpoint().startsWith(this.baseUrl), 'Endpoint must start with ' + this.baseUrl);
                ID caseId = CapacityPlanRestManager.updateCaseFields();
                res.setBody(caseId);
            }
            when else {
                System.assert(false, 'Invalid HTTP method ' + req.getMethod());
            }
        }

        return res;
    }

}