@isTest
private class CapacityPlanRestManagerAsyncTest {
    @testSetup static void setup() {
        // Create common test accounts
        List<String> listOrigins = new String[]{'Phone', 'Email', 'Web'};
        List<String> listStatus = new String[]{'High', 'Medium', 'Low'};
        List<String> listPriority = new String[]{'New', 'Working', 'Escalated', 'Closed'};
        List<Case> testCases = new List<Case>();

        for(Integer i=0;i<10;i++) {
            Case thisCase = new Case(
                Subject='Subject'+i,
                Status=listStatus[math.mod(i, 3)],
                Origin=listOrigins[math.mod(i, 3)],
                Priority=listPriority[math.mod(i, 4)]);

            testCases.add(thisCase);
        }
        insert testCases;        
    }

    @isTest static void testPost() {
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CapacityPlanRestManagerAsyncCalloutMock());

        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://org.my.salesforce.com/services/apexrest/CapacityPlanAsync');
        req.setMethod('POST');
        req.setBody('{ "status" : "New", "subject" : "Bigfoot Sighting!", "priority" : "Medium" , "origin" : "Web"}');
        Http h = new Http();
        HttpResponse res = h.send(req);

        Test.stopTest();
        System.assertEquals(200, res.getStatusCode());

        String contentType = res.getHeader('Content-Type');
        System.assert(contentType == 'application/json');

        String caseId = res.getBody();
        Case testCase = [SELECT ID, CaseNumber, Subject, Status, Origin, Priority, UpdateCounter__C FROM CASE WHERE Id = :caseId];
        System.assertEquals(testCase.Subject, 'Bigfoot Sighting!');
        List<AggregateResult> caseNumbers = [SELECT COUNT(Id) cntId FROM CASE];
        System.assertEquals(11, caseNumbers[0].get('cntId'));
    }

    @isTest static void testGet() {
        Case testCase = [SELECT ID, CaseNumber, Subject, Status, Origin, Priority, UpdateCounter__C FROM CASE LIMIT 1];

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CapacityPlanRestManagerAsyncCalloutMock());

        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://org.my.salesforce.com/services/apexrest/CapacityPlanAsync/' + testCase.Id);
        req.setMethod('GET');
        Http h = new Http();
        HttpResponse res = h.send(req);

        Test.stopTest();
        System.assertEquals(200, res.getStatusCode());

        String contentType = res.getHeader('Content-Type');
        System.assert(contentType == 'application/json');

        Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
        String caseId = params.get('Id').toString();
        Case returnCase = [SELECT ID, CaseNumber, Subject, Status, Origin, Priority, UpdateCounter__C FROM CASE WHERE Id = :caseId];
        System.assertEquals(testCase.Id, returnCase.Id);
        System.assertEquals(testCase.CaseNumber, returnCase.CaseNumber);
        System.assertEquals(testCase.Subject, returnCase.Subject);
        System.assertEquals(testCase.Status, returnCase.Status);
        System.assertEquals(testCase.Origin, returnCase.Origin);
        System.assertEquals(testCase.Priority, returnCase.Priority);
        System.assertEquals(testCase.UpdateCounter__C, returnCase.UpdateCounter__C);

    }

    @isTest static void testPut() {
        
        Case testCase = [SELECT ID, CaseNumber, Subject, Status, Origin, Priority, UpdateCounter__C FROM CASE LIMIT 1];

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CapacityPlanRestManagerAsyncCalloutMock());

        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://org.my.salesforce.com/services/apexrest/CapacityPlanAsync');
        req.setMethod('PUT');
        req.setBody('{ "status" : "New", "subject" : "Bigfoot Sighting!", "priority" : "Medium" , "origin" : "Web", "id" : "' + testCase.Id + '"}');
        Http h = new Http();
        HttpResponse res = h.send(req);

        Test.stopTest();
        System.assertEquals(200, res.getStatusCode());

        String contentType = res.getHeader('Content-Type');
        System.assert(contentType == 'application/json');

        String caseId = res.getBody();
        testCase = [SELECT ID, CaseNumber, Subject, Status, Origin, Priority, UpdateCounter__C FROM CASE WHERE Id = :caseId];
        System.assertEquals(testCase.Subject, 'Bigfoot Sighting!');
        List<AggregateResult> caseNumbers = [SELECT COUNT(Id) cntId FROM CASE];
        System.assertEquals(10, caseNumbers[0].get('cntId'));
    }

    @isTest static void testPutNew() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CapacityPlanRestManagerAsyncCalloutMock());

        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://org.my.salesforce.com/services/apexrest/CapacityPlanAsync');
        req.setMethod('PUT');
        req.setBody('{ "status" : "New", "subject" : "Bigfoot Sighting!", "priority" : "Medium" , "origin" : "Web"}');
        Http h = new Http();
        HttpResponse res = h.send(req);

        Test.stopTest();
        System.assertEquals(200, res.getStatusCode());

        String contentType = res.getHeader('Content-Type');
        System.assert(contentType == 'application/json');

        String caseId = res.getBody();
        Case testCase = [SELECT ID, CaseNumber, Subject, Status, Origin, Priority, UpdateCounter__C FROM CASE WHERE Id = :caseId];
        System.assertEquals(testCase.Subject, 'Bigfoot Sighting!');
        List<AggregateResult> caseNumbers = [SELECT COUNT(Id) cntId FROM CASE];
        System.assertEquals(11, caseNumbers[0].get('cntId'));
    }

    @isTest static void testPatchCounterNull() {
        Case testCase = [SELECT ID, CaseNumber, Subject, Status, Origin, Priority, UpdateCounter__C FROM CASE LIMIT 1];
        testCase.UpdateCounter__C = null;
        update testCase;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CapacityPlanRestManagerAsyncCalloutMock());

        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://org.my.salesforce.com/services/apexrest/CapacityPlanAsync/' + testCase.Id);
        req.setMethod('PATCH');
        req.setBody('{ "status" : "New", "subject" : "Bigfoot Sighting!", "priority" : "Medium" , "origin" : "Web"}');
        Http h = new Http();
        HttpResponse res = h.send(req);

        Test.stopTest();
        System.assertEquals(200, res.getStatusCode());

        String contentType = res.getHeader('Content-Type');
        System.assert(contentType == 'application/json');

        String caseId = res.getBody();
        testCase = [SELECT ID, CaseNumber, Subject, Status, Origin, Priority, UpdateCounter__C FROM CASE WHERE Id = :caseId];
        System.assertEquals(testCase.Subject, 'Bigfoot Sighting!');
        System.assertEquals(testCase.UpdateCounter__C, 1);
    }

    @isTest static void testPatch() {
        Case testCase = [SELECT ID, CaseNumber, Subject, Status, Origin, Priority, UpdateCounter__C FROM CASE LIMIT 1];
        testCase.UpdateCounter__C = 0;
        update testCase;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CapacityPlanRestManagerAsyncCalloutMock());

        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://org.my.salesforce.com/services/apexrest/CapacityPlanAsync/' + testCase.Id);
        req.setMethod('PATCH');
        req.setBody('{ "status" : "New", "subject" : "Bigfoot Sighting!", "priority" : "Medium" , "origin" : "Web"}');
        Http h = new Http();
        HttpResponse res = h.send(req);

        Test.stopTest();
        System.assertEquals(200, res.getStatusCode());

        String contentType = res.getHeader('Content-Type');
        System.assert(contentType == 'application/json');

        String caseId = res.getBody();
        testCase = [SELECT ID, CaseNumber, Subject, Status, Origin, Priority, UpdateCounter__C FROM CASE WHERE Id = :caseId];
        System.assertEquals(testCase.Subject, 'Bigfoot Sighting!');
        System.assertEquals(testCase.UpdateCounter__C, 1);
    }

    @isTest static void testDelete() {
        Case testCase = [SELECT ID, CaseNumber, Subject, Status, Origin, Priority, UpdateCounter__C FROM CASE LIMIT 1];

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CapacityPlanRestManagerAsyncCalloutMock());

        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://org.my.salesforce.com/services/apexrest/CapacityPlanAsync/' + testCase.Id);
        req.setMethod('DELETE');
        Http h = new Http();
        HttpResponse res = h.send(req);

        Test.stopTest();
        
        System.assertEquals(200, res.getStatusCode());
        String contentType = res.getHeader('Content-Type');
        System.assert(contentType == 'application/json');

        try{

            Case returnCase = [SELECT ID, CaseNumber, Subject, Status, Origin, Priority, UpdateCounter__C FROM CASE WHERE Id = :testCase.Id];
        }catch(Exception e){
            System.assert(e.getMessage().contains('List has no rows for assignment to SObject'), 'Different Error message: ' + e.getMessage());
        }
        
        List<AggregateResult> caseNumbers = [SELECT COUNT(Id) cntId FROM CASE];
        System.assertEquals(9, caseNumbers[0].get('cntId'));
    }
}