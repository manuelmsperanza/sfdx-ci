@isTest
private class SkillAssessmentControllerTest {
    
    @TestSetup
    static void makeData(){
        List<Worker__c> workForce = new List<Worker__c>();

        workForce.add(new Worker__c(Name='BUHead1'));
        workForce.add(new Worker__c(Name='PM1'));
        workForce.add(new Worker__c(Name='TeamLead1'));
        workForce.add(new Worker__c(Name='Analyst1'));
        workForce.add(new Worker__c(Name='Analyst2'));
        workForce.add(new Worker__c(Name='Analyst3'));
        workForce.add(new Worker__c(Name='Analyst4'));
        workForce.add(new Worker__c(Name='TeamLead2'));
        workForce.add(new Worker__c(Name='Consultant1'));
        workForce.add(new Worker__c(Name='Consultant2'));
        workForce.add(new Worker__c(Name='Consultant3'));
        workForce.add(new Worker__c(Name='Consultant4'));
        workForce.add(new Worker__c(Name='PM2'));
        workForce.add(new Worker__c(Name='TeamLead3'));
        workForce.add(new Worker__c(Name='Developer1'));
        workForce.add(new Worker__c(Name='Developer2'));
        workForce.add(new Worker__c(Name='Developer3'));
        workForce.add(new Worker__c(Name='TeamLead4'));
        workForce.add(new Worker__c(Name='Tester1'));
        workForce.add(new Worker__c(Name='Tester2'));
        workForce.add(new Worker__c(Name='Tester3'));
        workForce.add(new Worker__c(Name='Tester4'));
        workForce.add(new Worker__c(Name='PM3'));
        workForce.add(new Worker__c(Name='TeamLead5'));
        workForce.add(new Worker__c(Name='Operation1'));
        workForce.add(new Worker__c(Name='Operation2'));
        workForce.add(new Worker__c(Name='Operation3'));

        insert workForce;

        List<Skill__c> skillList = new List<Skill__c>();

        skillList.add(new Skill__c(Name='Creating a new Application'));
        skillList.add(new Skill__c(Name='Creating a new object'));
        skillList.add(new Skill__c(Name='Creating a new tab'));
        skillList.add(new Skill__c(Name='Creating new simple fields', Description__C='Creating new simple fields (text, data)'));
        skillList.add(new Skill__c(Name='Editing layout of object and sections'));
        skillList.add(new Skill__c(Name='Creating formula fields'));
        skillList.add(new Skill__c(Name='Creating relationships with other objects', Description__C='Creating relationships with other objects (both master-detail and lookup)'));
        skillList.add(new Skill__c(Name='Creating roll-up summary fields'));
        skillList.add(new Skill__c(Name='Enabling field history tracking'));
        skillList.add(new Skill__c(Name='Creating a record'));
        skillList.add(new Skill__c(Name='RecordType attribution to profiles'));
        skillList.add(new Skill__c(Name='Layout profile assignment based on record type'));
        skillList.add(new Skill__c(Name='Configuration of Profiles and Permission Sets', Description__C='Configuration of Profiles and Permission Sets (App Assignment, Object Permissions, Field Level Security)'));
        skillList.add(new Skill__c(Name='Sharing and Visibility Configuration', Description__C='Sharing and Visibility Configuration (Organization-Wide-Defaults, Sharing rules, Manual Sharing, Role hierarchy)'));
        skillList.add(new Skill__c(Name='Creating a Workflow with email sending action', Description__C='Creating a Workflow with email sending action (via template)'));
        skillList.add(new Skill__c(Name='Creating a Workflow with task creation action'));
        skillList.add(new Skill__c(Name='Creating a Time Workflow with field update'));
        skillList.add(new Skill__c(Name='Creating a grouping report'));
        skillList.add(new Skill__c(Name='Creating and use of Report Type'));
        skillList.add(new Skill__c(Name='Editing App Builder with Lightning Components'));
        skillList.add(new Skill__c(Name='Apex Explorer o Force.com Explorer'));
        skillList.add(new Skill__c(Name='SOQL, SOSL, Describe, DML'));
        skillList.add(new Skill__c(Name='Excel Connector'));
        skillList.add(new Skill__c(Name='Apex Data Loader', Description__C='Apex Data Loader (study well Upsert via External ID)'));
        skillList.add(new Skill__c(Name='Workbench'));
        skillList.add(new Skill__c(Name='DataBasin', Description__C='DataBasin (Tool interno WR )'));
        skillList.add(new Skill__c(Name='Access'));
        skillList.add(new Skill__c(Name='Force.com IDE / Visual Studio Code'));
        skillList.add(new Skill__c(Name='Salesforce DX'));
        skillList.add(new Skill__c(Name='Versioning (GIT)'));
        skillList.add(new Skill__c(Name='Atlassian: JIRA, Confluence, Bitbucket'));
        skillList.add(new Skill__c(Name='Governor Limits'));
        skillList.add(new Skill__c(Name='Sandbox'));
        skillList.add(new Skill__c(Name='Trigger After and Before'));
        skillList.add(new Skill__c(Name='Bulk Triggers'));
        skillList.add(new Skill__c(Name='Testing Class'));
        skillList.add(new Skill__c(Name='Debug & Deploy (IDE & Change Set)'));
        skillList.add(new Skill__c(Name='API Enterprise'));
        skillList.add(new Skill__c(Name='API Partner'));
        skillList.add(new Skill__c(Name='Custom Settings & Custom Metadata Types'));
        skillList.add(new Skill__c(Name='Visualforce UI'));
        skillList.add(new Skill__c(Name='Lightning Aura Components', Description__C='Lightning Aura Components (including Events)'));
        skillList.add(new Skill__c(Name='Lightning Web Components', Description__C='Lightning Web Components (including Events)'));
        skillList.add(new Skill__c(Name='Lightning Design System e usability'));
        skillList.add(new Skill__c(Name='Apex in AJAX'));
        skillList.add(new Skill__c(Name='Batch Apex'));
        skillList.add(new Skill__c(Name='Apex Scheduler'));
        skillList.add(new Skill__c(Name='Inbound email'));
        skillList.add(new Skill__c(Name='WebService Inbound', Description__C='WebService Inbound (Callin) REST and SOAP, both development and invocation'));
        skillList.add(new Skill__c(Name='WebService Outbound', Description__C='WebService Outbound (Callout) REST and SOAP, both development and reception'));
        skillList.add(new Skill__c(Name='Sites'));
        skillList.add(new Skill__c(Name='Communities'));
        skillList.add(new Skill__c(Name='Mobile'));
        skillList.add(new Skill__c(Name='Platform Events'));

        insert skillList;

    }

    @isTest private static void testGetRelatedSkills(){

        Worker__c worker = [SELECT ID FROM Worker__c LIMIT 1];
        List<Skill__c> listSkill = [SELECT ID, Name FROM Skill__c];
        WorkerSkill__c workerSkill = new WorkerSkill__c(Name=listSkill[0].Name, Worker__c=worker.Id, Skill__c=listSkill[0].Id);
        insert workerSkill;

        Test.startTest();
        List<WorkerSkill__c> listWorkerSkill =  SkillAssessmentController.getRelatedSkills(worker.Id);
        Test.stopTest();

        System.assertEquals(listWorkerSkill.size(), listSkill.size());
        
    }
}