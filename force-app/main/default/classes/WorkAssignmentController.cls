public with sharing class WorkAssignmentController {

    private static List<WorkAssignmentPeriodController> periodTokens = new List<WorkAssignmentPeriodController>();

    private static void fillPeriodToken(Date startDate, Date endDate, List<Object> columnsList) {
        Date periodStartDate = startDate;
        Date nextDate = null;
        periodTokens.clear();

        for(Integer columnListIdx = 1; columnListIdx < columnsList.size(); columnListIdx++){
            Date periodEndDate = null;
            if(columnListIdx+1 < columnsList.size()){
                Map<String, Object> nextColumn = (Map<String, Object>) columnsList.get(columnListIdx+1);
                String fieldName = (String) nextColumn.get('fieldName');
                nextDate = Date.valueOf(fieldName);
                periodEndDate = nextDate.addDays(-1);
            } else {
                periodEndDate = endDate;
            }
            WorkAssignmentPeriodController periodEntry = new WorkAssignmentPeriodController(periodStartDate, periodEndDate, 0);
            periodTokens.add(periodEntry);
            periodStartDate = nextDate;
        }
    }

    @AuraEnabled(cacheable=true)
    public static String getUsersAssigments(Date startDate, Date endDate, String scale, String columns) {
        List<Object> columnsList = (List<Object>) JSON.deserializeUntyped(columns);

        fillPeriodToken(startDate, endDate, columnsList);

        List<Worker__c> workerList = [SELECT Name,
            (SELECT Name, Effort__c, Engagement__c, StartDate__c, EndDate__c, WorkActivity__r.Name
            FROM WorkAssignments__r
            WHERE (StartDate__c <= :startDate AND (EndDate__c >= :startDate OR EndDate__c = null)) OR
            (StartDate__c <= :endDate AND (EndDate__c >= :endDate OR EndDate__c = null)) OR
            (StartDate__c >= :startDate AND EndDate__c <= :endDate))
            FROM Worker__c ORDER BY Name];
        JSONGenerator gen = JSON.createGenerator(false);
        gen.writeStartArray();

        for(Worker__c curWorker : workerList) {

            gen.writeStartObject();
            gen.writeStringField('Worker', curWorker.Name);

            List<WorkAssignmentPeriodController> listAssignments = new WorkAssignmentPeriodController[periodTokens.size()];
            for(WorkAssignment__c curWorkAssignment : curWorker.WorkAssignments__r){
                System.debug(curWorker.Name + ': ' + curWorkAssignment.Name + ' ' + curWorkAssignment.Engagement__c + ' ' + curWorkAssignment.StartDate__c + ' ' + curWorkAssignment.EndDate__c);
                for(Integer curPeriodTokenIdx = 0; curPeriodTokenIdx < periodTokens.size(); curPeriodTokenIdx++){
                    WorkAssignmentPeriodController wapc = listAssignments.get(curPeriodTokenIdx);
                    Date periodStartDate = periodTokens.get(curPeriodTokenIdx).startDate;
                    Date periodEndDate = periodTokens.get(curPeriodTokenIdx).endDate;
                    if(wapc == null){
                        wapc = new WorkAssignmentPeriodController(periodStartDate, periodEndDate, 0);
                        listAssignments.set(curPeriodTokenIdx, wapc);
                    }                
                    //Decimal engagementSum = wapc.engagementSum;
                    
                    if(curWorkAssignment.StartDate__c <= periodStartDate && curWorkAssignment.EndDate__c >= periodStartDate) {
                        wapc.engagementSum += curWorkAssignment.Engagement__c;
                    } else if(curWorkAssignment.StartDate__c <= periodStartDate && curWorkAssignment.EndDate__c == null) {
                        wapc.engagementSum += curWorkAssignment.Engagement__c;
                    } else if (curWorkAssignment.StartDate__c <= periodEndDate && curWorkAssignment.EndDate__c >= periodEndDate) {
                        wapc.engagementSum += curWorkAssignment.Engagement__c;
                    } else if (curWorkAssignment.StartDate__c <= periodEndDate && curWorkAssignment.EndDate__c == null) {
                        wapc.engagementSum += curWorkAssignment.Engagement__c;
                    } else if (curWorkAssignment.StartDate__c >= periodStartDate && curWorkAssignment.EndDate__c <= periodEndDate) {
                        wapc.engagementSum += curWorkAssignment.Engagement__c;
                    }    
                    //listAssignments.get(curPeriodTokenIdx).engagementSum = engagementSum;
                    //listAssignments.set(curPeriodTokenIdx, wapc);
                    System.debug('Analyze period from ' + periodStartDate + ' to ' + periodEndDate + ' curAssignValue ' + wapc.engagementSum);

                }
            }

            for(Integer columnListIdx = 1; columnListIdx < columnsList.size(); columnListIdx++){
                Map<String, Object> curColumn = (Map<String, Object>) columnsList.get(columnListIdx);
                String fieldName = (String) curColumn.get('fieldName');
                if(listAssignments.get(columnListIdx-1) == null){
                    
                    gen.writeNumberField(fieldName, 0);
                } else{
                    Decimal engagementSum = listAssignments.get(columnListIdx-1).engagementSum;
                    gen.writeNumberField(fieldName, engagementSum/100);

                }
            }

            gen.writeEndObject();
        }

        gen.writeEndArray();

        return gen.getAsString();
    }

    @AuraEnabled(cacheable=true)
    public static List<WorkAssignment__c> getUserAssigmentsDetail(Date startDate, Date endDate, String selectedUsers) {

        List<String> listUsers = new List<String>();
        for(Object curObject : (List<Object>) JSON.deserializeUntyped(selectedUsers)){
            listUsers.add((String) curObject);
        }  
        return getUsersDetail(startDate, endDate, listUsers);
        

    }

    public static List<WorkAssignment__c> getUsersDetail(Date startDate, Date endDate, List<String> listUsers) {
        if(endDate == null) {
            return [SELECT Name, Worker__r.Name, Effort__c, Engagement__c, StartDate__c, EndDate__c,
                WorkActivity__r.Name, WorkActivity__r.WorkPackage__r.Name, WorkActivity__r.Project__r.Name,
                (SELECT Id, StartDate__c, EndDate__c, WARTicket__c, WorkAssignment__c  FROM WorkAssignmentTickets__r)
                FROM WorkAssignment__c
                WHERE Worker__r.Name IN :listUsers
                AND ((StartDate__c <= :startDate AND (EndDate__c >= :startDate OR EndDate__c = null)) OR StartDate__c >= :startDate )
                ORDER BY Worker__r.Name, StartDate__c, EndDate__c];
        } else {
            return [SELECT Name, Worker__r.Name, Effort__c, Engagement__c, StartDate__c, EndDate__c,
                WorkActivity__r.Name, WorkActivity__r.WorkPackage__r.Name, WorkActivity__r.Project__r.Name,
                (SELECT Id, StartDate__c, EndDate__c, WARTicket__c, WorkAssignment__c  FROM WorkAssignmentTickets__r)
                FROM WorkAssignment__c
                WHERE Worker__r.Name IN :listUsers
                AND ((StartDate__c <= :startDate AND (EndDate__c >= :startDate OR EndDate__c = null)) OR
                (StartDate__c <= :endDate AND (EndDate__c >= :endDate OR EndDate__c = null)) OR
                (StartDate__c >= :startDate AND EndDate__c <= :endDate))
                ORDER BY Worker__r.Name, StartDate__c, EndDate__c];
        }
    }
    
    private static WorkAssignmentPeriodController cloneOverlapping(Date startDate, Date endDate, WorkAssignmentPeriodController overlapping, WorkAssignment__c workAssignment){
        WorkAssignmentPeriodController newOl = new WorkAssignmentPeriodController(startDate, endDate, 0);
        for(WorkAssignment__c olWa : overlapping.listWorkAssignment){
            newOl.addWorkAssignment(olWa);
        }
        newOl.addWorkAssignment(workAssignment);
        return newOl;
    }

    public static void checkNewAssignment(WorkAssignment__c workAssignment) {

        List<String> listUsers = new List<String>();
        listUsers.add(workAssignment.Worker__r.Name);
        List<WorkAssignment__c> listConcurrentWorkAssignment = getUsersDetail(workAssignment.StartDate__c, workAssignment.EndDate__c, listUsers);

        Date overLappingStartDate = null;
        Date overLappingEndDate = null;
        List<WorkAssignmentPeriodController> overlappingList = new List<WorkAssignmentPeriodController>();
        List<WorkAssignmentTicket__c> listWorkAssignmentTicket = new List<WorkAssignmentTicket__c>();
        for(WorkAssignment__c curWorkAssignment : listConcurrentWorkAssignment){

            listWorkAssignmentTicket.addAll(curWorkAssignment.WorkAssignmentTickets__r);

            if(curWorkAssignment.Id <> workAssignment.Id){
                if(workAssignment.StartDate__c > curWorkAssignment.StartDate__c) {
                    overLappingStartDate = workAssignment.StartDate__c;
                } else {
                    overLappingStartDate = curWorkAssignment.StartDate__c;
                }
    
                if(workAssignment.EndDate__c == null) {
                    overLappingEndDate = curWorkAssignment.EndDate__c;
                } else if(curWorkAssignment.EndDate__c == null) {
                    overLappingEndDate = workAssignment.EndDate__c;
                } else if(workAssignment.EndDate__c < curWorkAssignment.EndDate__c) {
                    overLappingEndDate = workAssignment.EndDate__c;
                } else {
                    overLappingEndDate = curWorkAssignment.EndDate__c;
                }
    
                System.debug('Assignment ' + curWorkAssignment.Name + ' overlap in ' + overLappingStartDate + ' and ' + overLappingEndDate);
    
                List<WorkAssignmentPeriodController> overlappingItemToAdd = new List<WorkAssignmentPeriodController>();

                for(WorkAssignmentPeriodController overlappingItem : overlappingList) {

                    if(overLappingStartDate < overlappingItem.startDate) {
                        if(overLappingEndDate == null || overLappingEndDate > overlappingItem.startDate) {
                            WorkAssignmentPeriodController wapcOl = new WorkAssignmentPeriodController(overLappingStartDate, overlappingItem.startDate.addDays(-1), 0);
                            wapcOl.addWorkAssignment(workAssignment);
                            wapcOl.addWorkAssignment(curWorkAssignment);
                            overlappingItemToAdd.add(wapcOl);
                            overLappingStartDate = overlappingItem.startDate;
                        }
                    }

                    if(overLappingStartDate == overlappingItem.startDate) {
                        if(overLappingEndDate != null && (overlappingItem.endDate == null || overLappingEndDate < overlappingItem.endDate)) {
                            overlappingItemToAdd.add(cloneOverlapping(overLappingStartDate, overLappingEndDate, overlappingItem, curWorkAssignment));

                            overlappingItem.startDate = overLappingEndDate.addDays(1);
                            overLappingStartDate = null;

                        } else {
                            overlappingItem.addWorkAssignment(curWorkAssignment);
                            if(overLappingEndDate == overlappingItem.endDate){
                                overLappingStartDate = null;
                            } else {
                                overLappingStartDate = overlappingItem.endDate.addDays(1);
                            }
                        }
                    }

                    if(overLappingStartDate > overlappingItem.startDate && (overlappingItem.endDate == null || overLappingStartDate <= overlappingItem.endDate)) {
                        if(overLappingEndDate == overlappingItem.endDate){
                            overlappingItemToAdd.add(cloneOverlapping(overLappingStartDate, overLappingEndDate, overlappingItem, curWorkAssignment));

                            overlappingItem.endDate = overLappingStartDate.addDays(-1);
                            overLappingStartDate = null;
                        } else if(overLappingEndDate != null && (overlappingItem.endDate == null || overLappingEndDate < overlappingItem.endDate)) {

                            overlappingItemToAdd.add(cloneOverlapping(overlappingItem.startDate, overLappingStartDate.addDays(-1), overlappingItem, null));
                            overlappingItemToAdd.add(cloneOverlapping(overLappingEndDate.addDays(1), overlappingItem.endDate, overlappingItem, null));
                            overlappingItem.addWorkAssignment(curWorkAssignment);
                            overlappingItem.startDate = overLappingStartDate;
                            overlappingItem.endDate = overLappingEndDate;
                            overLappingStartDate = null;

                        } else if(overlappingItem.endDate != null && (overLappingEndDate == null || overlappingItem.endDate < overLappingEndDate)) {
                            overlappingItemToAdd.add(cloneOverlapping(overLappingStartDate, overLappingEndDate, overlappingItem, curWorkAssignment));

                            overlappingItem.endDate = overLappingStartDate.addDays(-1);
                            overLappingStartDate = overlappingItem.endDate.addDays(1);
                        }
                    }
                }
    
                if(overLappingStartDate != null) {
                    WorkAssignmentPeriodController wapcOl = new WorkAssignmentPeriodController(overLappingStartDate, overLappingEndDate, 0);
                    wapcOl.addWorkAssignment(workAssignment);
                    wapcOl.addWorkAssignment(curWorkAssignment);
                    overlappingItemToAdd.add(wapcOl);   
                }
    
                if(overlappingItemToAdd.size() > 0) {
                    overlappingList.addAll(overlappingItemToAdd);
                    overlappingList.sort();
                }
            }   
        }
        List<WorkAssignmentTicket__c> listWorkAssignmentTicketToDelete = new List<WorkAssignmentTicket__c>();
        Set<Id> setWARTicketToDelete = new Set<Id>();
        for(WorkAssignmentTicket__c curWorkAssignmentTicked : listWorkAssignmentTicket) {
            boolean toBeDeleted = true;
            for (WorkAssignmentPeriodController curOverlappingItem : overlappingList) {
                if(curWorkAssignmentTicked.StartDate__c == curOverlappingItem.startDate && curWorkAssignmentTicked.EndDate__c == curOverlappingItem.endDate){
                    if(curOverlappingItem.engagementSum > 100){
                        toBeDeleted = false;
                    }
                }
            }
            if(toBeDeleted){
                listWorkAssignmentTicketToDelete.add(curWorkAssignmentTicked);
                setWARTicketToDelete.add(curWorkAssignmentTicked.WARTicket__c);
            }
        }
        delete listWorkAssignmentTicketToDelete;
        WARTicket__c[] warTickets = [SELECT Id FROM WARTicket__c WHERE Id IN :setWARTicketToDelete];
        delete warTickets;

        List<WorkAssignmentTicket__c> listWorkAssignmentTicketToCreate = new List<WorkAssignmentTicket__c>();
        List<WARTicket__c> listWARTicketToCreate = new List<WARTicket__c>();
        Map<WARTicket__c, List<WorkAssignmentTicket__c>> mapWARAssign = new Map<WARTicket__c, List<WorkAssignmentTicket__c>>();
        for (WorkAssignmentPeriodController curOverlappingItem : overlappingList) {
            if(curOverlappingItem.engagementSum > 100){
                List<WorkAssignmentTicket__c> listExistentWorkAssignmentTicket = new List<WorkAssignmentTicket__c>();
                for(WorkAssignmentTicket__c curWorkAssignmentTicked : listWorkAssignmentTicket) {
                    if(curWorkAssignmentTicked.StartDate__c == curOverlappingItem.startDate && curWorkAssignmentTicked.EndDate__c == curOverlappingItem.endDate){
                        listExistentWorkAssignmentTicket.add(curWorkAssignmentTicked);
                    }
                }
      
                if(listExistentWorkAssignmentTicket.size() == 0){
                    
                    WARTicket__c newWARTicket = new WARTicket__c(Message__c='Overlapping from ' + curOverlappingItem.startDate + ' to ' + curOverlappingItem.endDate);
                    listWARTicketToCreate.add(newWARTicket);
                    List<WorkAssignmentTicket__c> subListWorkAssignTicket = new List<WorkAssignmentTicket__c>();
                    for(WorkAssignment__c curWorkAssignmentToTicket : curOverlappingItem.listWorkAssignment){
                        WorkAssignmentTicket__c newWorkAssignmentTicket = new WorkAssignmentTicket__c(StartDate__c = curOverlappingItem.startDate,
                                EndDate__c = curOverlappingItem.endDate,
                                WorkAssignment__c = curWorkAssignmentToTicket.Id);
                        subListWorkAssignTicket.add(newWorkAssignmentTicket);
                        listWorkAssignmentTicketToCreate.add(newWorkAssignmentTicket);
                    }
                    mapWARAssign.put(newWARTicket, subListWorkAssignTicket);
                }else {
                    Id refWARTicket = listExistentWorkAssignmentTicket.get(0).WARTicket__c;
                
                    for(WorkAssignment__c curWorkAssignmentToTicket : curOverlappingItem.listWorkAssignment){
                        boolean toBeInsert = true;
                        for(WorkAssignmentTicket__c curWorkAssignmentTicket: listExistentWorkAssignmentTicket) {
                            if(curWorkAssignmentTicket.WorkAssignment__c == curWorkAssignmentToTicket.Id){
                                toBeInsert = false;
                            }
                        }
                        if(toBeInsert){
                            WorkAssignmentTicket__c newWorkAssignmentTicket = new WorkAssignmentTicket__c(StartDate__c = curOverlappingItem.startDate,
                                EndDate__c = curOverlappingItem.endDate,
                                WARTicket__c = refWARTicket,
                                WorkAssignment__c = curWorkAssignmentToTicket.Id);
                            listWorkAssignmentTicketToCreate.add(newWorkAssignmentTicket);
                        }
                    }
                }
            }
        }
        insert listWARTicketToCreate;
        for(WARTicket__c curWarTicket : mapWARAssign.keySet()) {
            for(WorkAssignmentTicket__c curWorkAssignTicket : mapWARAssign.get(curWarTicket)){
                curWorkAssignTicket.WARTicket__c = curWarTicket.Id;
            }
        }
        insert listWorkAssignmentTicketToCreate;

    }

    @AuraEnabled(cacheable=true)
    public static List<Project__c> getProjects() {
        return [SELECT Id, Name FROM Project__c ORDER BY Name];
    }

    @AuraEnabled(cacheable=true)
    public static List<WorkPackage__c> getWorkPackages(Id projectId) {
        return [SELECT Id, Name FROM WorkPackage__c WHERE Project__c = :projectId ORDER BY Name];
    }

    @AuraEnabled(cacheable=true)
    public static List<WorkActivity__c> getWorkActivities(Id workPackageId) {
        return [SELECT Id, Name, PlannedStartDate__c, PlannedEndDate__c, 
            ActualStartDate__c, ActualEndDate__c
            FROM WorkActivity__c WHERE WorkPackage__c = :workPackageId ORDER BY Name];
    }

}