global class WorkAssignmentPeriodController implements Comparable {
    public Date startDate;
    public Date endDate;
    public Decimal engagementSum = 0;
    public List<WorkAssignment__c> listWorkAssignment = new List<WorkAssignment__c>();

    public WorkAssignmentPeriodController() {

    }

    public WorkAssignmentPeriodController(Date startDate, Date endDate, Decimal engagementSum) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.engagementSum = engagementSum;
    }

    public void addWorkAssignment(WorkAssignment__c workAssignment){
        if(workAssignment != null){
            if(workAssignment.Engagement__c != null){
                this.engagementSum += workAssignment.Engagement__c;
            }
            listWorkAssignment.add(workAssignment);
        }

    }

    global Integer compareTo(Object compareTo) {
        // Cast argument to OpportunityWrapper
        WorkAssignmentPeriodController compareToWAPC = (WorkAssignmentPeriodController)compareTo;
        
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        if (this.startDate > compareToWAPC.startDate) {
            // Set return value to a positive value.
            returnValue = 1;
        } else if (this.startDate < compareToWAPC.startDate) {
            // Set return value to a negative value.
            returnValue = -1;
        } else {
            if(this.endDate == null && compareToWAPC.endDate == null) {
                returnValue = 0;
            } else if(this.endDate == null && compareToWAPC.endDate != null){
                returnValue = 1;
            } else if(this.endDate != null && compareToWAPC.endDate == null){
                returnValue = -1;
            } else if(this.endDate > compareToWAPC.endDate){
                returnValue = 1;
            } else if(this.endDate < compareToWAPC.endDate){
                returnValue = -1;
            }
        }
        if(returnValue == 0){
            if(this.engagementSum > compareToWAPC.engagementSum){
                returnValue = 1;
            } else if(this.engagementSum < compareToWAPC.engagementSum) {
                returnValue = -1;
            }
        }
        return returnValue;       
    }

}