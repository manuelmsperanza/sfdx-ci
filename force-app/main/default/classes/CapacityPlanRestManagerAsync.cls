@RestResource(urlMapping='/CapacityPlanAsync/*')
global with sharing class CapacityPlanRestManagerAsync {
    @HttpGet
    global static Case getCaseById() {
        RestRequest request = RestContext.request;
        // grab the caseId from the end of the URL
        String caseId = request.requestURI.substring(
          request.requestURI.lastIndexOf('/')+1);
        Case result =  [SELECT CaseNumber,Subject,Status,Origin,Priority, UpdateCounter__C
                        FROM Case
                        WHERE Id = :caseId];
        return result;
    }
    @HttpPost
    global static ID createCase(String subject, String status,
        String origin, String priority) {
        Case thisCase = new Case(
            Subject=subject,
            Status=status,
            Origin=origin,
            Priority=priority);
        insert thisCase;
        return thisCase.Id;
    }   
    @HttpDelete
    global static void deleteCase() {
        RestRequest request = RestContext.request;
        String caseId = request.requestURI.substring(
            request.requestURI.lastIndexOf('/')+1);
        Case thisCase = [SELECT Id FROM Case WHERE Id = :caseId];
        delete thisCase;
    }     
    @HttpPut
    global static ID upsertCase(String subject, String status,
        String origin, String priority, String id) {
        Case thisCase = new Case(
                Id=id,
                Subject=subject,
                Status=status,
                Origin=origin,
                Priority=priority);
        // Match case by Id, if present.
        // Otherwise, create new case.
        upsert thisCase;
        // Return the case ID.
        return thisCase.Id;
    }
    @HttpPatch
    global static ID updateCaseFields() {
        RestRequest request = RestContext.request;
        String caseId = request.requestURI.substring(
            request.requestURI.lastIndexOf('/')+1);

        Case thisCase = [SELECT Id FROM Case WHERE Id = :caseId];

        updateCounter(thisCase.Id, request.requestbody.tostring());

        return thisCase.Id;
    } 

    @future
    public static void updateCounter(ID caseId, String requestBody)
    {   
        
        Case thisCase = [SELECT Id, UpdateCounter__C FROM Case WHERE Id = :caseId];
        // Deserialize the JSON string into name-value pairs
        //System.debug(request.requestbody.tostring());
        Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(requestBody);
        // Iterate through each parameter field and value
        for(String fieldName : params.keySet()) {
            // Set the field and value on the Case sObject
            thisCase.put(fieldName, params.get(fieldName));
        }

        if(thisCase.UpdateCounter__C == null){
            thisCase.UpdateCounter__C = 1;
        } else {
            thisCase.UpdateCounter__C = thisCase.UpdateCounter__C + 1;
        }
        
        update thisCase;
    }

}