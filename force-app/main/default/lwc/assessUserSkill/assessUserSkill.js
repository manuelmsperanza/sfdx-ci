import { LightningElement, api, wire } from 'lwc';
import { createRecord, deleteRecord  } from 'lightning/uiRecordApi';
import { refreshApex } from '@salesforce/apex';
import getRelatedSkills from '@salesforce/apex/SkillAssessmentController.getRelatedSkills';
import WORKER_SKILL_OBJECT from '@salesforce/schema/WorkerSkill__c';
import WORKER_SKILL_NAME_FIELD from '@salesforce/schema/WorkerSkill__c.Name';
import WORKER_SKILL_SKILL_FIELD from '@salesforce/schema/WorkerSkill__c.Skill__c';
import WORKER_SKILL_WORKER_FIELD from '@salesforce/schema/WorkerSkill__c.Worker__c';

export default class AssessUserSkill extends LightningElement {

    @api recordId;
    @wire(getRelatedSkills, { workerId: '$recordId' })
    loadUserSkills(result) {
        console.log('loadUserSkills: ' + JSON.stringify(result));
        this.userSkills = result;
        console.log('loadUserSkills: ' + JSON.stringify(this.userSkills));
    }
    //@wire(getRelatedSkills, { workerId: '$recordId' })
    userSkills;
    
    assessSkills() {
        console.log('assessSkills');
        
        const checkList = this.template.querySelectorAll('lightning-input[data-element="assessment-checkbox"]');
        checkList.forEach( (currentValue, index, array) => {
            
            var workerSkill = this.userSkills.data[index];
            
            if(currentValue.checked && workerSkill.Id == null) {
                const fields = {};
                fields[WORKER_SKILL_NAME_FIELD.fieldApiName] = workerSkill.Name;
                fields[WORKER_SKILL_SKILL_FIELD.fieldApiName] = workerSkill.Skill__c;
                fields[WORKER_SKILL_WORKER_FIELD.fieldApiName] = this.recordId;
                const recordInput = { apiName: WORKER_SKILL_OBJECT.objectApiName, fields };

                //currentValue.checked = false;

                createRecord(recordInput)
                    .then((workerSkillRecord) => {
                        refreshApex(this.userSkills)
                    });

            } else if(!currentValue.checked && workerSkill.Id != null) {
                console.log('delete ' + workerSkill.Id);
                deleteRecord(workerSkill.Id)
                    .then(() => {
                        refreshApex(this.userSkills)
                    });
            }
        }, this);
    }

}