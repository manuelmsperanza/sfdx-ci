import { LightningElement, api, wire } from 'lwc';
//import { FlowAttributeChangeEvent } from 'lightning/flowSupport';
import getProjects from '@salesforce/apex/WorkAssignmentController.getProjects';
import getWorkPackages from '@salesforce/apex/WorkAssignmentController.getWorkPackages';
import getWorkActivities from '@salesforce/apex/WorkAssignmentController.getWorkActivities';

export default class AssignWorkActivity extends LightningElement {

    @api recordId;
    @api worker;
    @api projectId;
    @api workPackageId;
    @api workActivity;
    isProjectUnset = true;
    isWorkPackageUnset = true;
    isWorkActivityUnset = true;

    @wire(getProjects)
    loadProjects(result) {
        //console.log('loadProjects: ' + JSON.stringify(result));
        if(result.data == undefined || result.data == null || result.data.length == 0){
            this.projectsList = null
            this.isProjectUnset = true;
        } else{
            this.projectsList = new Array(result.data.length);
            result.data.forEach( (currentValue, index , array) => {
                //console.log('loadProjects add ' + JSON.stringify(currentValue));
                this.projectsList[index] = { label: currentValue.Name, value: currentValue.Id};
            }, this);
        }
        //console.log('loadProjects done');
    }
    projectsList;

    @wire(getWorkPackages, {projectId : '$projectId'})
    loadWorkPackages(result) {
        //console.log('loadWorkPackages: ' + JSON.stringify(result));
        if(result.data == undefined || result.data == null || result.data.length == 0){
            this.workPackagesList = null
            this.isWorkPackageUnset = true;
        } else{
            this.workPackagesList = new Array(result.data.length);
            result.data.forEach( (currentValue, index , array) => {
                this.workPackagesList[index] = { label: currentValue.Name, value: currentValue.Id};
            }, this);
        }
    }
    workPackagesList;

    @wire(getWorkActivities, {workPackageId : '$workPackageId'})
    loadWorkActivities(result) {
        //console.log('loadWorkActivities: ' + JSON.stringify(result));
        if(result.data == undefined || result.data == null || result.data.length == 0){
            this.workActivitiesList = null
            this.isWorkActivityUnset = true;
        } else{
            this.workActivitiesList = new Array(result.data.length);
            result.data.forEach( (currentValue, index , array) => {
                this.workActivitiesList[index] = { label: currentValue.Name, value: currentValue.Id, object: currentValue};
            }, this);
        }
    }
    workActivitiesList;

    handleSelectProject(event) {
        this.projectId = event.detail.value;
        this.isProjectUnset = false;
    }

    handleSelectWorkPackage(event) {
        this.workPackageId = event.detail.value;
        this.isWorkPackageUnset = false;
    }
    
    handleSelectWorkActivity(event) {
        this.workActivityId = event.detail.value;

        this.workActivitiesList.forEach( (currentValue, index , array) => {
            if(currentValue.value == this.workActivityId){
                this.workActivity = currentValue.object;
            }
        }, this);

        this.isWorkActivityUnset = false;
        //const attributeChangeEvent = new FlowAttributeChangeEvent('workActivity', this.workActivity);
        //this.dispatchEvent(attributeChangeEvent);
    }
    
}