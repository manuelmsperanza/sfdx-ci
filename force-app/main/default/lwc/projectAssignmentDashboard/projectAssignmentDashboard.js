import { LightningElement, api, wire, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import getUsersAssigments from '@salesforce/apex/WorkAssignmentController.getUsersAssigments';
import getUserAssigmentsDetail from '@salesforce/apex/WorkAssignmentController.getUserAssigmentsDetail';

export default class ProjectAssignmentDashboard extends LightningElement {
    scale = 'week';
    startDateCriteria;
    endDateCriteria;
    @api startDate;
    @api endDate;
    @api selectedUsers;
    @api error;
    data;
    columns;
    isStartDateUnset = true;
    isEndDateUnset = true;
    isPeriodUnset = true;
    isUserUnselect = true;

    @wire(getUserAssigmentsDetail, {startDate : '$startDate', endDate : '$endDate', selectedUsers : '$selectedUsers'})
    loadUserAssignments(result) {
        //console.log('loadUserAssignments: ' + JSON.stringify(result));
        if(result.data == undefined || result.data == null || result.data.length == 0){
            this.isUserUnselect = true
        } else{
            this.isUserUnselect = false
            this.userAssignments = new Array(result.data.length);
            result.data.forEach( (currentValue, index , array) => {
                this.userAssignments[index] = { Worker__Name : currentValue.Worker__r.Name, 
                    WorkActivity__Name : currentValue.WorkActivity__r.Name,
                    WorkPackage__Name : currentValue.WorkActivity__r.WorkPackage__r.Name,
                    Project__Name : currentValue.WorkActivity__r.Project__r.Name,
                    Effort__c : Math.round(currentValue.Effort__c),
                    Engagement__c : currentValue.Engagement__c/100,
                    StartDate__c : currentValue.StartDate__c,
                    EndDate__c : currentValue.EndDate__c};
            }, this);
        }
    }
    userAssignments;

    columnsUserAssignment = [
        {label: 'Worker', fieldName: 'Worker__Name', type: 'text'},
        {label: 'Activity', fieldName: 'WorkActivity__Name', type: 'text'},
        {label: 'Work Package', fieldName: 'WorkPackage__Name', type: 'text'},
        {label: 'Project', fieldName: 'Project__Name', type: 'text'},
        {label: 'Effort', fieldName: 'Effort__c', type: 'number'},
        {label: 'Engagement', fieldName: 'Engagement__c', type: 'percent'},
        {label: 'StartDate', fieldName: 'StartDate__c', type: 'date'},
        {label: 'EndDate', fieldName: 'EndDate__c', type: 'date'}
   ];

    getSelectedUser(event) {
        console.log('getSelectedUser');
        //console.log('event ' + JSON.stringify(event));
        const selectedRows = event.detail.selectedRows;
        
        var listUsers = new Array(selectedRows.length);
        // Display that fieldName of the selected rows
        for (let selectedRowIdx = 0; selectedRowIdx < selectedRows.length; selectedRowIdx++){
            console.log('add ' + selectedRows[selectedRowIdx].Worker);
            listUsers[selectedRowIdx] = selectedRows[selectedRowIdx].Worker;
        }
        this.selectedUsers = JSON.stringify(listUsers);
        //console.log('selectedUsers' + this.selectedUsers);
    }

    handleData() {
        console.log('handleData');
        if(!this.isStartDateUnset && !this.isEndDateUnset) {
            //console.log('columns ' + JSON.stringify(this.columns));
            getUsersAssigments({startDate : this.startDate, endDate: this.endDate, scale: this.scale, columns : JSON.stringify(this.columns)})
                .then(result => {
                    //console.log('result ' + result);
                    this.data = JSON.parse(result);
                })
                .catch(error => {
                    this.error = JSON.stringify(error);
                    console.log('error ' + this.error)
                });
        }
    }

    get timeScaleOptions() {
        return [
            { label: 'Day', value: 'day' },
            { label: 'Week', value: 'week' },
            { label: 'Fortnight', value: 'fortnight' },
            { label: 'Month', value: 'month' }
        ];
    }

    handleTimeScaleChange(event) {
        console.log('handleTimeScaleChange');
        this.scale = event.detail.value;
        this.normalizePeriodValues();
        this.createTableValues();
    }

    handleStartDateCriteriaChange(event) {
        console.log('handleStartDateCriteriaChange');
        this.startDateCriteria = event.detail.value;
        if(this.startDateCriteria != undefined && this.startDateCriteria != null){
            this.startDate = new Date(this.startDateCriteria);
            this.isStartDateUnset = false;
        } else {
            this.startDate = null;
            this.isStartDateUnset = true;
        }
        this.isPeriodUnset = (this.isStartDateUnset || this.isEndDateUnset);
        this.checkPeriodValues();
        this.normalizeStartDate();
        this.createTableValues();
    }

    handleEndDateCriteriaChange(event) {
        console.log('handleEndDateCriteriaChange');
        this.endDateCriteria = event.detail.value;
        if(this.endDateCriteria != undefined && this.endDateCriteria != null){
            this.endDate = new Date(this.endDateCriteria);
            this.isEndDateUnset = false;
        } else {
            this.endDate = null;
            this.isEndDateUnset = true;
        }
        this.isPeriodUnset = (this.isStartDateUnset || this.isEndDateUnset);
        this.checkPeriodValues();
        this.normalizeEndDate();
        this.createTableValues();
    }

    getDateForInput( dateToFormat) {
        console.log('getDateForInput');
        return dateToFormat.getFullYear() + '-' + (dateToFormat.getMonth() + 1) + '-' + dateToFormat.getDate();
    }

    checkPeriodValues() {
        console.log('checkPeriodValues');
        //if(this.startDate != undefined && this.startDate != null && this.endDate != undefined && this.endDate != null){
        if(!this.isStartDateUnset && !this.isEndDateUnset) {
            if(this.startDate.getTime() > this.endDate.getTime()) {

                const evtError = new ShowToastEvent({
                    title: "Wrong date choose",
                    message: 'You cannot set the start date after the end date',
                    variant: 'error',
                    mode: 'dismissable'
                });
                this.dispatchEvent(evtError);

                const evtWarning = new ShowToastEvent({
                    title: "Start date is after the End date",
                    message: 'Let me swap the dates',
                    variant: 'warning',
                    mode: 'dismissable'
                });
                this.dispatchEvent(evtWarning);

                this.startDate = new Date(this.endDateCriteria);
                this.endDate = new Date(this.startDateCriteria);
            }
        }
    }

    normalizePeriodValues() {
        console.log('normalizePeriodValues');
        this.normalizeStartDate();
        this.normalizeEndDate();
    }

    normalizeStartDate() {
        console.log('normalizeStartDate');
        
        //if(this.startDate != undefined && this.startDate != null){
        if(!this.isStartDateUnset){
            switch(this.scale) {
                case 'week':
                    this.normalizeWeekStartDate();
                  break;
                case 'fortnight':
                    this.normalizeFortnightStartDate();
                  break;
                case 'month':
                    this.normalizeMonthStartDate();
                    break;       
            }

            var dateString = this.getDateForInput(this.startDate);
            this.startDateCriteria = dateString;
        }
    }

    normalizeEndDate() {
        console.log('normalizeEndDate');
        //if(this.endDate != undefined && this.endDate != null){
        if(!this.isEndDateUnset){
            switch(this.scale) {
                case 'week':
                    this.normalizeWeekEndDate();
                  break;
                case 'fortnight':
                    this.normalizeFortnightEndDate();
                  break;
                case 'month':
                    this.normalizeMonthEndDate();
                    break;       
            }

            var dateString = this.getDateForInput(this.endDate);
            this.endDateCriteria = dateString;
        }
    }

    createTableValues() {
        console.log('createTableValues');
        switch(this.scale) {
            case 'day' : 
                this.createDailyTable();
              break;
            case 'week':
                this.createWeekTable();
              break;
            case 'fortnight':
                this.createFortnightTable();
              break;
            case 'month':
                this.createMonthTable();
                break;       
        }
        console.log('Invoke handleData');
        this.handleData();
    }

    createDailyTable() {
        console.log('createDailyTable');
        //if(this.startDate != undefined && this.startDate != null && this.endDate != undefined && this.endDate != null){
        if(!this.isStartDateUnset && !this.isEndDateUnset) {
            const diffTime = this.endDate - this.startDate;
            const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24))+1;
            var arrayLength = diffDays + 1;
            this.columns = new Array(arrayLength);
            this.columns[0] = { label: 'Worker', fieldName: 'Worker', type: 'text' };
            var columnDate = new Date(this.startDate);
            for (let i = 1; i < arrayLength; i++) {
                var dateString = this.getDateForInput(columnDate);
                this.columns[i] = { label: dateString, fieldName: dateString, type: 'percent' };
                columnDate.setDate(columnDate.getDate() + 1);
            }
        }
    }
    
    normalizeWeekStartDate() {
        console.log('normalizeWeekStartDate');
        var startDateDayOfWeek = this.startDate.getDay();
        if(startDateDayOfWeek != 1) {
            this.startDate.setDate((this.startDate.getDate()-startDateDayOfWeek+1));
        }
    }

    normalizeWeekEndDate() {
        console.log('normalizeWeekEndDate');
        var endDateDayOfWeek = this.endDate.getDay();
        if(endDateDayOfWeek != 0) {
            this.endDate.setDate((this.endDate.getDate()+7-endDateDayOfWeek));
        }
    }

    defineWeeksColumns() {
        console.log('defineWeeksColumns');
        const diffTime = this.endDate - this.startDate;
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24))+1; 
        var arrayLength = diffDays/7 + 1;
        //console.log('arrayLength ' + arrayLength);
        this.columns = new Array(arrayLength);
        this.columns[0] = { label: 'Worker', fieldName: 'Worker', type: 'text' };

        var columnDate = new Date(this.startDate);
        for (let i = 1; i < arrayLength; i++) {
            var dateString = this.getDateForInput(columnDate);
            this.columns[i] = { label: dateString, fieldName: dateString, type: 'percent' };
            columnDate.setDate(columnDate.getDate() + 7);
        }
        //console.log('defineWeeksColumns columns ' + JSON.stringify(this.columns));
    }

    createWeekTable() {
        console.log('createWeekTable');
        /*var isStartDateSet = false;
        if(this.startDate != undefined && this.startDate != null){
            isStartDateSet = true;
            //this.normalizeWeekStartDate();
        }
        
        var isEndDateSet = false;
        if(this.endDate != undefined && this.endDate != null){
            isEndDateSet = true;
            //this.normalizeWeekEndDate();
        }

        if(isStartDateSet && isEndDateSet){*/
        if(!this.isStartDateUnset && !this.isEndDateUnset){
            this.defineWeeksColumns();
        }
        //console.log('createWeekTable columns ' + JSON.stringify(this.columns));
    }

    normalizeFortnightStartDate() {
        console.log('normalizeFortnightStartDate');
        if(this.startDate.getDate() <= 15) {
            this.startDate.setDate(1);
        } else {
            this.startDate.setDate(16);
        }
    }
    
    firstDayOfNextMonth( inDate ) {
        console.log('firstDayOfNextMonth');
        inDate.setDate(1);
        if(inDate.getMonth() < 11){
            inDate.setMonth(inDate.getMonth() + 1);
        } else {
            inDate.setFullYear(inDate.getFullYear() + 1);
            inDate.setMonth(0);
        }
        console.log('inDate ' + inDate);
    }

    calculateEndOfMonth( inDate ) {
        console.log('calculateEndOfMonth');
        /*if(inDate.getMonth() < 11){
            inDate.setMonth(inDate.getMonth() + 1);
        } else {
            inDate.setFullYear(inDate.getFullYear() + 1);
            inDate.setMonth(0);
        }*/
        this.firstDayOfNextMonth(inDate);
        inDate.setDate(inDate.getDate() - inDate.getDate());
        console.log('inDate ' + inDate);
    }

    normalizeFortnightEndDate() {
        console.log('normalizeFortnightEndDate');
        if(this.endDate.getDate() <= 15) {
            this.endDate.setDate(15);
        } else {
            /*if(this.endDate.getMonth() < 11){
                this.endDate.setMonth(this.endDate.getMonth() + 1);
            } else {
                this.endDate.setFullYear(this.endDate.getFullYear() + 1);
                this.endDate.setMonth(0);
            }
            this.endDate.setDate(this.endDate.getDate() - this.endDate.getDate());*/
            this.calculateEndOfMonth(this.endDate);
            console.log('endDate ' + this.endDate);
        }
    }

    defineFortnightsColumns() {
        console.log('defineFortnightsColumns');
        this.columns = new Array(1);
        this.columns[0] = { label: 'Worker', fieldName: 'Worker', type: 'text' };
        var columnDate = new Date(this.startDate);
        do {
            var dateString = this.getDateForInput(columnDate);
            this.columns.push({ label: dateString, fieldName: dateString, type: 'percent' });
            if(columnDate.getDate() <= 15) {
                columnDate.setDate(16);
            } else {
                this.firstDayOfNextMonth(columnDate);
                //columnDate.setDate(1);
            }            
        } while (columnDate.getTime() < this.endDate.getTime());
    }

    createFortnightTable() {
        console.log('createFortnightTable');
        /*var isStartDateSet = false;
        if(this.startDate != undefined && this.startDate != null){
            isStartDateSet = true;
            //this.normalizeFortnightStartDate();
        }
        var isEndDateSet = false;
        if(this.endDate != undefined && this.endDate != null){
            isEndDateSet = true;
            //this.normalizeFortnightEndDate();
        }

        if(isStartDateSet && isEndDateSet){*/
        if(!this.isStartDateUnset && !this.isEndDateUnset) {
            this.defineFortnightsColumns();
        }
    }

    normalizeMonthStartDate() {
        console.log('normalizeMonthStartDate');
        this.startDate.setDate(1);
    }

    normalizeMonthEndDate() {
        console.log('normalizeMonthEndDate');
        /*if(this.endDate.getMonth() < 11){
            this.endDate.setMonth(this.endDate.getMonth() + 1);
        } else {
            this.endDate.setFullYear(this.endDate.getFullYear() + 1);
            this.endDate.setMonth(0);
        }
        this.endDate.setDate(this.endDate.getDate() - this.endDate.getDate());*/
        this.calculateEndOfMonth(this.endDate);
    }

    defineMonthsColumns() {
        console.log('defineMonthsColumns');
        const diffYear = this.endDate.getFullYear() - this.startDate.getFullYear();
        const diffMonth = diffYear * 12 + (this.endDate.getMonth() - this.startDate.getMonth()) + 1;
        var arrayLength = diffMonth + 1;
        this.columns = new Array(arrayLength);
        this.columns[0] = { label: 'Worker', fieldName: 'Worker', type: 'text' };
        var columnDate = new Date(this.startDate);
        for (let i = 1; i < arrayLength; i++) {
            var dateString = this.getDateForInput(columnDate);
            this.columns[i] = { label: dateString, fieldName: dateString, type: 'percent' };
            this.firstDayOfNextMonth(columnDate);
        }
    }

    createMonthTable() {
        console.log('createMonthTable');
        /*var isStartDateSet = false;
        if(this.startDate != undefined && this.startDate != null){
            isStartDateSet = true;
            //this.normalizeMonthStartDate();
        }
        var isEndDateSet = false;
        if(this.endDate != undefined && this.endDate != null){
            isEndDateSet = true;
            //this.normalizeMonthEndDate();
        }

        if(isStartDateSet && isEndDateSet){*/
        if(!this.isStartDateUnset && !this.isEndDateUnset) {
            this.defineMonthsColumns();
        }

    }

    getNextStartDate() {
        console.log('getNextStartDate');
        //if(this.startDate != undefined && this.startDate != null){
        if(!this.isStartDateUnset){
            var newStartDate = new Date(this.startDate);
            switch(this.scale) {
                case 'day':
                    newStartDate.setDate(newStartDate.getDate() + 1);
                    break;
                case 'week':
                    newStartDate.setDate(newStartDate.getDate() + 7);
                break;
                case 'fortnight':
                    if(newStartDate.getDate() <= 15) {
                        newStartDate.setDate(16);
                    } else {
                        /*if(newStartDate.getMonth() < 11){
                            newStartDate.setMonth(newStartDate.getMonth() + 1);
                        } else {
                            newStartDate.setFullYear(newStartDate.getFullYear() + 1);
                            newStartDate.setMonth(0);
                        }*/
                        this.firstDayOfNextMonth(newStartDate);
                        //newStartDate.setDate(1);
                    }
                break;
                case 'month':
                    /*if(newStartDate.getMonth() < 11){
                        newStartDate.setMonth(newStartDate.getMonth() + 1);
                    } else {
                        newStartDate.setFullYear(newStartDate.getFullYear() + 1);
                        newStartDate.setMonth(0);
                    }*/

                    this.firstDayOfNextMonth(newStartDate);

                    break;       
            }
            //if(this.endDate != undefined && this.endDate != null && newStartDate.getTime() > this.endDate.getTime()) {
            if(!this.isEndDateUnset && newStartDate.getTime() > this.endDate.getTime()) {
                const evtError = new ShowToastEvent({
                    title: "Wrong date choose",
                    message: 'You cannot set the start date after the end date',
                    variant: 'error',
                    mode: 'dismissable'
                });
                this.dispatchEvent(evtError);
            } else {
                this.startDate = new Date(newStartDate);
                var dateString = this.getDateForInput(this.startDate);
                this.startDateCriteria = dateString;

            }
        }
    }

    subtractAMonth( inDate ) {
        console.log('subtractAMonth');
        if(inDate.getMonth() > 0){
            inDate.setMonth(inDate.getMonth() - 1);
        } else {
            inDate.setFullYear(inDate.getFullYear() - 1);
            inDate.setMonth(11);
        }
    }

    getPrevStartDate() {
        console.log('getPrevStartDate');
        //if(this.startDate != undefined && this.startDate != null){
        if(!this.isStartDateUnset){
            var newStartDate = new Date(this.startDate);
            switch(this.scale) {
                case 'day':
                    newStartDate.setDate(newStartDate.getDate() - 1);
                    break;
                case 'week':
                    newStartDate.setDate(newStartDate.getDate() - 7);
                break;
                case 'fortnight':
                    if(newStartDate.getDate() > 15) {
                        newStartDate.setDate(1);
                    } else {
                        /*if(newStartDate.getMonth() > 0){
                            newStartDate.setMonth(newStartDate.getMonth() - 1);
                        } else {
                            newStartDate.setFullYear(newStartDate.getFullYear() - 1);
                            newStartDate.setMonth(11);
                        }*/
                        this.subtractAMonth(newStartDate);
                        newStartDate.setDate(16);
                    }
                break;
                case 'month':
                    /*if(newStartDate.getMonth() > 0){
                        newStartDate.setMonth(newStartDate.getMonth() - 1);
                    } else {
                        newStartDate.setFullYear(newStartDate.getFullYear() - 1);
                        newStartDate.setMonth(11);
                    }*/
                    this.subtractAMonth(newStartDate);
                    break;       
            }
            
            this.startDate = new Date(newStartDate);
            var dateString = this.getDateForInput(this.startDate);
            this.startDateCriteria = dateString;
        }
    }
    
    getNextEndDate(){
        console.log('getNextEndDate');
        //if(this.endDate != undefined && this.endDate != null){
        if(!this.isEndDateUnset) {
            var newEndDate = new Date(this.endDate);
            switch(this.scale) {
                case 'day':
                    newEndDate.setDate(newEndDate.getDate() + 1);
                    break;
                case 'week':
                    newEndDate.setDate(newEndDate.getDate() + 7);
                break;
                case 'fortnight':
                    if(newEndDate.getDate() > 15) {
                        newEndDate.setDate(newEndDate.getDate() + 1);
                        newEndDate.setDate(15);
                    } else {
                        /*if(newEndDate.getMonth() < 11){
                            newEndDate.setMonth(newEndDate.getMonth() + 1);
                        } else {
                            newEndDate.setFullYear(newEndDate.getFullYear() + 1);
                            newEndDate.setMonth(0);
                        }
                        this.firstDayOfNextMonth(newEndDate);
                        newEndDate.setDate(newEndDate.getDate() - newEndDate.getDate());*/
                        this.calculateEndOfMonth(newEndDate);
                    }
                break;
                case 'month':
                    newEndDate.setDate(newEndDate.getDate() + 1);
                    /*if(newEndDate.getMonth() < 11){
                        newEndDate.setMonth(newEndDate.getMonth() + 1);
                    } else {
                        newEndDate.setFullYear(newEndDate.getFullYear() + 1);
                        newEndDate.setMonth(0);
                    }
                    newEndDate.setDate(newEndDate.getDate() - newEndDate.getDate());*/
                    this.calculateEndOfMonth(newEndDate);
                    break;       
            }
            
            this.endDate = new Date(newEndDate);
            var dateString = this.getDateForInput(this.endDate);
            this.endDateCriteria = dateString;
        }
    }
    
    getPrevEndDate() {
        console.log('getPrevEndDate');
        //if(this.endDate != undefined && this.endDate != null){
        if(!this.isEndDateUnset) {
            var newEndDate = new Date(this.endDate);
            switch(this.scale) {
                case 'day':
                    newEndDate.setDate(newEndDate.getDate() - 1);
                    break;
                case 'week':
                    newEndDate.setDate(newEndDate.getDate() - 7);
                break;
                case 'fortnight':
                    if(newEndDate.getDate() > 15) {
                        newEndDate.setDate(15);
                    } else {
                        newEndDate.setDate(newEndDate.getDate() - newEndDate.getDate());
                    }
                break;
                case 'month':
                    newEndDate.setDate(newEndDate.getDate() - newEndDate.getDate());
                    break;       
            }
            //if(this.startDate != undefined && this.startDate != null && this.startDate.getTime() > newEndDate.getTime()) {
            if(!this.isStartDateUnset != null && this.startDate.getTime() > newEndDate.getTime()) {
                const evtError = new ShowToastEvent({
                    title: "Wrong date choose",
                    message: 'You cannot set the end date before the start date',
                    variant: 'error',
                    mode: 'dismissable'
                });
                this.dispatchEvent(evtError);
            } else {
                this.endDate = new Date(newEndDate);
                var dateString = this.getDateForInput(this.endDate);
                this.endDateCriteria = dateString;
            }
        }
    }

    increaseStartDate() {
        console.log('increaseStartDate');
        this.getNextStartDate();
        this.createTableValues();
    }
    decreaseStartDate() {
        console.log('decreaseStartDate');
        this.getPrevStartDate();
        this.createTableValues();
    }
    increaseEndDate() {
        console.log('increaseEndDate');
        this.getNextEndDate();
        this.createTableValues();
    }
    decreaseEndDate() {
        console.log('decreaseEndDate');
        this.getPrevEndDate();
        this.createTableValues();
    }

    shiftPeriodBackward() {
        console.log('shiftPeriodBackward');
        this.getPrevStartDate();
        this.getPrevEndDate();
        this.createTableValues();
    }

    shiftPeriodForward() {
        console.log('shiftPeriodForward');
        this.getNextStartDate();
        this.getNextEndDate();
        this.createTableValues();
    }

}